package university.jala.activity_2.main;

public class Validator {

  private static Validator instance;
  private String[] args;
  private Character typeOfValue;
  private String[] values;
  private String[] options = {"t=", "v="};

  private Validator() {
  }

  public static Validator getInstance() {
    if (instance == null) {
      synchronized (Repository.class) {
        if (instance == null) {
          instance = new Validator();
        }
      }
    }
    return instance;
  }

  public void setTypeOfValue(Character typeOfValue) {
    this.typeOfValue = typeOfValue;
  }

  public void setArgs(String[] args) {
    this.args = args;
  }

  /**
   *
   */
  private void getValues() {
    for (String arg : this.args) {
      if (arg != null && arg.contains(options[0]) && validTypeOfValue(
          arg.charAt(arg.length() - 1))) {
        setTypeOfValue(arg.charAt(arg.length() - 1));
      } else if (arg.contains(options[1]) && arg != null) {
      }
    }
  }

  public boolean validTypeOfValue(Character key) {
    return Repository.getInstance().getTypeOfValue().containsKey(key);
  }


  public boolean validValues(String[] args) {
    String[] possibleNumberValues = {"-9", "-8", "-7", "-6", "-5", "-4", "-3", "-2", "-1", "0", "1",
        "2", "3", "4", "5", "6", "7", "8", "9"};

    String possibleLettersValues = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (String arg : args) {
      boolean isValid = false;
      if (Repository.getInstance().getTypeOfValue().get(typeOfValue)) {
        for (String number : possibleNumberValues) {
          if (arg == number) {
            isValid = true;
            break;
          }
        }
        if (!isValid) {
          return false;
        }
      } else if (!Repository.getInstance().getTypeOfValue().get(typeOfValue)) {
        for (String letter : possibleLettersValues.split("")) {
          if (arg.equals(letter)) {
            isValid = true;
            break;
          }
        }
        if (!isValid) {
          return false;
        }
      } else {
        return false;
      }
    }
    return true;
  }
}
