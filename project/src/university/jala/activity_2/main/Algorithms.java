package university.jala.activity_2.main;

public enum Algorithms {
  INSERTION,
  BUBBLE,
  MERGE
}
