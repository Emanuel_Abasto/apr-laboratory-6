package university.jala.activity_2.main;

import java.util.Map;
import java.util.HashMap;

public class Repository {

  private static Repository instance;
  private Map<Character, Boolean> typeOfValue;

  private Repository() {
    typeOfValue = new HashMap<>();
    defaultTypeOfValue();
  }

  public static Repository getInstance() {
    if (instance == null) {
      synchronized (Repository.class) {
        if (instance == null) {
          instance = new Repository();
        }
      }
    }
    return instance;
  }

  public Map<Character, Boolean> getTypeOfValue() {
    return typeOfValue;
  }

  private void defaultTypeOfValue() {
    typeOfValue.put('n', true);
    typeOfValue.put('N', true);
    typeOfValue.put('c', false);
    typeOfValue.put('C', false);
  }
}
