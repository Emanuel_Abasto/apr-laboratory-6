package university.jala.activity_2.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import university.jala.activity_2.main.Validator;

public class RepositoryTest {

  @Test
  public void validTypeOfValueTest() {
    assertEquals(true, Validator.getInstance().validTypeOfValue('c'));
    assertEquals(false, Validator.getInstance().validTypeOfValue('f'));
  }

  @Test
  public void validValuesTest() {
    String[] valuesToCheck = {"1", "1", "3", "9"};
    Validator.getInstance().setTypeOfValue('N');
    assertEquals(true, Validator.getInstance().validValues(valuesToCheck));

    valuesToCheck[0] = "A";
    assertEquals(false, Validator.getInstance().validValues(valuesToCheck));

    Validator.getInstance().setTypeOfValue('C');
    String[] valuesToCheck2 = {"D","c","B","b","A"};
    assertEquals(true, Validator.getInstance().validValues(valuesToCheck2));
  }
}
