package university.jala.activity_1.main;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class WordsCounter {
  public static Map wordsCounter(String[] args) {
    Map<String, Integer> words = new LinkedHashMap<>();
    for (String arg : args) {
      if (words.containsKey(arg)) {
        words.put(arg, words.get(arg) + 1);
      } else {
        words.put(arg, 1);
      }
    }
    return words;
  }

  public static String wordPrinter(Map<String, Integer> map) {
    return map.entrySet()
        .stream()
        .map(entry -> entry.getKey() + ", " + entry.getValue())
        .collect(Collectors.joining("\n\n"));
  }

  public static Map wordsCounter(String string) {
    String[] args = string.split(" ");
    return wordsCounter(args);
  }
}