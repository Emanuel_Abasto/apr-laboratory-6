package university.jala.activity_1.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import university.jala.activity_1.main.WordsCounter;

public class MainTest {

  @Test
  public void wordPrinterTest() {
    String message = "Esta secuencia de palabras de entrada tiene varias palabras repetidas";
    assertEquals(
        "Esta, 1\n" + "\n" + "secuencia, 1\n" + "\n" + "de, 2\n" + "\n" + "palabras, 2\n" + "\n"
            + "entrada, 1\n" + "\n" + "tiene, 1\n" + "\n" + "varias, 1\n" + "\n" + "repetidas, 1"
        , WordsCounter.wordPrinter(WordsCounter.wordsCounter(message)));

    message = "1 1 2 3 2 3 2 3 5";

    assertEquals("1, 2\n" + "\n" + "2, 3\n" + "\n" + "3, 3\n" + "\n" + "5, 1",
        WordsCounter.wordPrinter(WordsCounter.wordsCounter(message)));
  }
}
